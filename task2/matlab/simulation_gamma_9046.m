function simulation_gamma_9046(init,I,m,r,k,J,mi)
global u_used; global s_used;
%% ode
opts=odeset('RelTol',1e-6,'AbsTol',1e-3);
[t,y] = ode23s(@(t,y) ode_gamma_9046(t,y),[0 30],init,opts);
%% desired, u and error arrays
desired=[(-0.1*ones(length(t),1)-0.5*sin(t))'; ...
    -0.5*cos(t)'; ...
    +0.5*sin(t)'; ...
    +0.5*cos(t)']';

for j=1:2
    error(:,j)=y(:,j)+desired(:,j);
end

%% Plots
% Plots designed for 1920x1080p
fig=figure('Position',[0 35 1920 965]);
set(fig,'name',strcat('I=', num2str(I), ', m=', num2str(m), ', g=9.8, r=', num2str(r), ...
    ', k=', num2str(k), ', J=', num2str(J), ', mi=', num2str(mi), ', Initial Configs: y=[', ...
    num2str(init(1)), ',', num2str(init(2)), ',', num2str(init(3)), ',', num2str(init(4)),']'));
for j=1:2
subplot(2,2,(j-1)*2+1)
plot(t,y(:,j))
hold on;
plot(t,desired(:,j))
hold off;
legend({['$y_' num2str(j) '$'],['$y_' num2str(j) '^d$']}, 'interpreter','latex', 'fontsize',15);
if(j==1),title({['$Position: y_' num2str(j) '$']}, 'interpreter','latex', 'fontsize',17)
else, title({['$Velocity: y_' num2str(j) '$']}, 'interpreter','latex', 'fontsize',17)
end
end
for j=1:2
subplot(2,2,(j-1)*2+2)
plot(t,error(:,j))
if(j==1),title({['$Error of Position: y_' num2str(j) '$']}, 'interpreter','latex', 'fontsize',17)
else, title({['$Error of Velocity: y_' num2str(j) '$']}, 'interpreter','latex', 'fontsize',17)
end
end

% Inside ode function much more values are computed than
% what it is actually returned, hence we 
% pick them by a specific step, which results in length(t) values
analogy_u=length(u_used)/length(t);
analogy_s=length(s_used)/length(t);
u_plot=u_used(1:analogy_u:length(u_used));
s_plot=s_used(1:analogy_s:length(s_used));
% these values are not 100% accurate, some are far off the result
% hence we try to normalize the plot, so that it can be realistic
% and easily readable
temp_s=s_plot(1);
for k=1:length(t)-1
    if abs(u_plot(k) - u_plot(k+1)) > 12
        u_plot(k+1)=u_plot(k);
    end
    if (abs(s_plot(k) - s_plot(k+1)) > 0.002 && abs(s_plot(k) - temp_s) > 0.002)
        temp_s=s_plot(k+1);
        s_plot(k+1)=s_plot(k);
    end
end

figure('Position',[0 35 1920 965]);
plot(t,u_plot)
title({['$u$']}, 'interpreter','latex', 'fontsize',17)

figure('Position',[0 35 1920 965]);
plot(t,s_plot)
title({['$s$']}, 'interpreter','latex', 'fontsize',17)
end
