function [dz] = ode_beta_9046(t,z)

     global a; global b; global c; global d; global e;
     global a_hat; global b_hat; global c_hat; global d_hat; global e_hat;
     
     w=a*cos(z(1))*(-2*a*sin(z(1))+2*b-c+z(2)^2) ...
          -a*sin(z(1))*(z(3)-d*z(2)) ...
          +z(3)*(b-c)-b*d*z(2)-d*z(4);
     w_hat=a_hat*cos(z(1))*(-2*a_hat*sin(z(1))+2*b_hat-c_hat+ ...
          z(2)^2) -a_hat*sin(z(1))*(z(3)-d_hat*z(2)) ...
          +z(3)*(b_hat-c_hat)-b_hat*d_hat*z(2)-d_hat*z(4);
     v=0.5*sin(t)-40*(z(4)+0.5*cos(t))-600*(z(3)+0.5*sin(t)) ...
          -4000*(z(2)-0.5*cos(t))-10000*(z(1)-0.1-0.5*sin(t));
     dz(1)=z(2);
     dz(2)=z(3);
     dz(3)=z(4);
     dz(4)=w+b*e*v/(b_hat*e_hat)-b*e*w_hat/(b_hat*e_hat);
     dz=dz';
end

