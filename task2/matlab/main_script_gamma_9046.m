%% Initializations
I=6.6; m=20; g=9.8; r=0.25; k=1900; J=6.18; mi=1;
global a; global b; global c; global d; global e;
a=m*g*r/I; b=k/I; c=k/J; d=mi/J; e=1/J;
init=[0;0;1;2];

I_hat=[I,10,7,4]; m_hat=[m,19,21.5,24]; g_hat=9.8; r_hat=[r,0.2,0.35,0.5];
k_hat=[k,1900,2200,2500]; J_hat=[J,10,7.5,5]; mi_hat=[mi,0.5,1.75,3];

global a_hat; global b_hat; global c_hat; global d_hat; global e_hat;
a_hat=m_hat(1)*g*r_hat(1)/I_hat(1); b_hat=k_hat(1)/I_hat(1); ...
     c_hat=k_hat(1)/J_hat(1); d_hat=mi_hat(1)/J_hat(1); e_hat=1/J_hat(1);
%% Max and Min values
a_max=m_hat(4)*g*r_hat(4)/I_hat(4);
a_min=m_hat(2)*g*r_hat(2)/I_hat(2);
b_max=k_hat(4)/I_hat(4);
b_min=k_hat(2)/I_hat(2);
c_max=k_hat(4)/J_hat(4);
c_min=k_hat(2)/J_hat(2);
d_max=mi_hat(4)/J_hat(4);
d_min=mi_hat(2)/J_hat(2);
e_max=1/J_hat(4);
e_min=1/J_hat(2);
global e1; global e2; global e3; global e4; 
global e5; global e6; global e7; global e8;  
%% Calculating u parameters
e1=max_diff(1/(b_min*e_min), 1/(b_max*e_max),1/(b*e));
e2=max_diff(a_max/(b_min*e_min), a_min/(b_max*e_max),a/(b*e));
e3=max_diff(2*a_max^2/(b_min*e_min), 2*a_min^2/(b_max*e_max),2*a^2/(b*e));
e4=max_diff(a_max*(2*b_max-c_min)/(b_min*e_min), a_min*(2*b_min-c_max)/(b_max*e_max),a*(2*b-c)/(b*e));
e5=max_diff((b_max-c_min)/(b_min*e_min),(b_min-c_max)/(b_max*e_max),(b-c)/(b*e));
e6=max_diff(d_max/(b_min*e_min), d_min/(b_max*e_max),d/(e));
e7=max_diff(d_max/(e_min), d_min/(e_max),d/(e));
e8=max_diff(a_max*d_max/(b_min*e_min), a_min*d_min/(b_max*e_max),a*d/(b*e));
%% Running the simulation
simulation_gamma_9046(init,I,m,r,k,J,mi);
