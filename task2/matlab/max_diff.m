function [return_max]=max_diff(maximum,minimum,hat)
return_max=abs(max(maximum-hat,minimum-hat));
end