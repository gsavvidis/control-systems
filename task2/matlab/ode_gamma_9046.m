function [dz] = ode_gamma_9046(t,z)
     global a; global b; global c; global d; global e;
     global a_hat; global b_hat; global c_hat; global d_hat; global e_hat;
     global e1; global e2; global e3; global e4; 
     global e5; global e6; global e7; global e8;
     global u_used; global s_used;
     lambda=2;
     var=0.01;
     epsilon=1;
     %% w(z)
     w=a*cos(z(1))*(-2*a*sin(z(1))+2*b-c+z(2)^2) ...
          -a*sin(z(1))*(z(3)-d*z(2)) ...
          +z(3)*(b-c)-b*d*z(2)-d*z(4);
     w_hat=a_hat*cos(z(1))*(-2*a_hat*sin(z(1))+2*b_hat-c_hat+ ...
          z(2)^2) -a_hat*sin(z(1))*(z(3)-d_hat*z(2)) ...
          +z(3)*(b_hat-c_hat)-b_hat*d_hat*z(2)-d_hat*z(4);
     %% s
     epsilon_0=z(1)-0.5*cos(t)-0.1;
     epsilon_1=z(2)+0.5*sin(t);
     epsilon_2=z(3)+0.5*cos(t);
     epsilon_3=z(4)-0.5*sin(t);
     s=epsilon_3+lambda*epsilon_2+3*lambda^2*epsilon_1+lambda^3*epsilon_0;
     s_used=[s_used s];
     %% u_eq
     rho1=e1*abs(0.5*cos(t)-3*lambda*epsilon_3-3*lambda^2*epsilon_2-lambda^3*epsilon_1);
     rho2=e2*abs(cos(z(1))*z(2)^2-sin(z(1))*z(3));
     rho3=e3*abs(cos(z(1))*sin(z(1)));
     rho4=e4*abs(cos(z(1)));
     rho5=e5*abs(z(3));
     rho6=e6*abs(z(4));
     rho7=e7*abs(z(2));
     rho8=e8*abs(z(2)*sin(z(1)));
     rho_add=rho1+rho2+rho3+rho4+rho5+rho6+rho7+rho8+var;
     
     u_eq=1/(b_hat*e_hat)*(0.5*cos(t)-w_hat-3*lambda*epsilon_3-3*lambda^2*epsilon_2-lambda^3*epsilon_1);
     %% u
     if abs(s)>=epsilon
         if s>0
            u=u_eq-rho_add;
         else
             u=u_eq+rho_add;
         end
     else
          u=u_eq-rho_add*s/epsilon;
     end
     u_used=[u_used u];
     %% dz
     dz(1)=z(2);
     dz(2)=z(3);
     dz(3)=z(4);
     dz(4)=w+b*e*u;
     dz=dz';
end

