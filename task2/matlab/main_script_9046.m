%% Initializations
I=6.6; m=20; g=9.8; r=0.25; k=1900; J=6.18; mi=1;
global a; global b; global c; global d; global e;
a=m*g*r/I; b=k/I; c=k/J; d=mi/J; e=1/J;
I_hat=[I,10,7,4]; m_hat=[m,19,21.5,24]; g_hat=9.8; r_hat=[r,0.2,0.35,0.5]; ...
     k_hat=[k,1900,2200,2500]; J_hat=[J,10,7.5,5]; mi_hat=[mi,0.5,1.75,3];
global a_hat; global b_hat; global c_hat; global d_hat; global e_hat;
%% Running the simulation
init=[0;0;1;2];
for i=1:4
     a_hat=m_hat(i)*g_hat*r_hat(i)/I_hat(i);
     b_hat=k_hat(i)/I_hat(i);
     c_hat=k_hat(i)/J_hat(i);
     d_hat=mi_hat(i)/J_hat(i);
     e_hat=1/J_hat(i);
     simulation_9046(init,I_hat(i),m_hat(i),r_hat(i),k_hat(i),J_hat(i),mi_hat(i));
end
