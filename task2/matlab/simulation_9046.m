function simulation_9046(init,I,m,r,k,J,mi)
%% ode
[t,y] = ode45(@(t,y) ode_beta_9046(t,y),[0 30],init);
%% desired, u and error arrays
desired=[(-0.1*ones(length(t),1)-0.5*sin(t))'; ...
    -0.5*cos(t)'; ...
    +0.5*sin(t)'; ...
    +0.5*cos(t)']';

v=0.5*sin(t)-40*(y(:,4)+0.5*cos(t))-600*(y(:,3)+0.5*sin(t)) ...
          -4000*(y(:,2)-0.5*cos(t))-10000*(y(:,1)-0.1-0.5*sin(t));
      
for j=1:2
    error(:,j)=y(:,j)+desired(:,j);
end

%% Plots
% Plots designed for 1920x1080p
fig=figure('Position',[0 35 1920 965]);
set(fig,'name',strcat('I=', num2str(I), ', m=', num2str(m), ', g=9.8, r=', num2str(r), ...
    ', k=', num2str(k), ', J=', num2str(J), ', mi=', num2str(mi), ', Initial Configs: y=[', ...
    num2str(init(1)), ',', num2str(init(2)), ',', num2str(init(3)), ',', num2str(init(4)),']'));
for j=1:2
subplot(2,2,(j-1)*2+1)
plot(t,y(:,j))
if(j==1),title({['$Position: y_' num2str(j) '$']}, 'interpreter','latex', 'fontsize',17)
else, title({['$Velocity: y_' num2str(j) '$']}, 'interpreter','latex', 'fontsize',17)
end
end
for j=1:2
subplot(2,2,(j-1)*2+2)
plot(t,error(:,j))
if(j==1),title({['$Error of Position: y_' num2str(j) '$']}, 'interpreter','latex', 'fontsize',17)
else, title({['$Error of Velocity: y_' num2str(j) '$']}, 'interpreter','latex', 'fontsize',17)
end
end

figure('Position',[0 35 1920 965]);
set(fig,'name',strcat('I=', num2str(I), ', m=', num2str(m), ', g=9.8, r=', num2str(r), ...
    ', k=', num2str(k), ', J=', num2str(J), ', mi=', num2str(mi), ', Initial Configs: y=[', ...
    num2str(init(1)), ',', num2str(init(2)), ',', num2str(init(3)), ',', num2str(init(4)),']'));
plot(t,v)
title({['$v$']}, 'interpreter','latex', 'fontsize',17)
end