%% Initializations
global c;
global d;
global k;
global M;
c=2;
d=3;
k=3;
M=3;
b=[1,2,2.1];    
init=[0.1,5,-18,150;0.1,-1.6,5,19];
%% main
for b_val=1:3
for ite=1:4
%% equilibrium calculations
if (b(b_val)>c)
    equil=[((b(b_val)-c)^2+d)/k;0];
else
    equil=[];
end
%% ode
[t,y] = ode45(@(t,y) ode9046(y,b(b_val)),[0 1000],init(:,ite));

%% Stabilization Condition [with a error margin of 1e-7]
stabilized=[0,0];
if ~isnan(equil)
     for i=1:(length(y(:,2))-1)
         if (abs(y(i,1)-y(i+1,1))<1e-7)
             stabilized(1)=i;
         end
         if (abs(y(i,2)-y(i+1,2))<1e-7)
             stabilized(2)=i;
         end
         if (stabilized(1)>0 && stabilized(2)>0)
             fprintf(['Stabilized for b = ',num2str(b(b_val)), ' and inital values: x_1 = ', ...
                 num2str(init(1,ite)),', x_2 = ' num2str(init(2,ite)), '\n'])
             break;
         end
     end
     % Only plots until the system is stabilized will be used
     t=t(1:max(stabilized));
     y=y(1:max(stabilized),:);
end

%% Plots
% These plots are designed for 1920x1080p display
% Phase Portrait Plots
if ite==1
    figure('Position',[0 540 960 460])
elseif ite==2
    figure('Position',[960 540 960 460])
elseif ite==3
    figure('Position',[0 20 960 460])
elseif ite==4
    figure('Position',[960 20 960 460])
end
plot(y(:,2),y(:,1))
title({'Phase Portrait',['$Initial Configs: x_1=' num2str(init(1,ite)) '  \&   x_2=' num2str(init(2,ite)) '$']}, 'interpreter','latex', 'fontsize',17);
legend({['b=' num2str(b(b_val))]});

% Time Response Plots
if ite==1
    figure('Position',[0 540 960 460])
elseif ite==2
    figure('Position',[960 540 960 460])
elseif ite==3
    figure('Position',[0 20 960 460])
elseif ite==4
    figure('Position',[960 20 960 460])
end
subplot(2,1,1)
plot(t,y(:,1))
title({['$Time Response of x_1$'],['$Initial Configs: x_1=' num2str(init(1,ite)) '$']}, 'interpreter','latex', 'fontsize',17)

if ~isnan(equil)
     line(get(gca,'XLim'), [equil(1) equil(1)], ...
          'Color','magenta', 'LineWidth', 0.5);
     text(t(length(t)), y(length(y),1),...
    {sprintf('t=%d s',round(t(length(t)))), "\downarrow"},'Color','red','FontSize',9 ...
    ,'VerticalAlignment','bottom')
end
legend({['b=' num2str(b(b_val))]});

subplot(2,1,2)
plot(t,y(:,2))
title({['$Time Response of x_2$'],['$Initial Configs: x_2=' num2str(init(2,ite)) '$']}, 'interpreter','latex', 'fontsize',17)
if ~isnan(equil)
     line(get(gca,'XLim'), [equil(2) equil(2)], ...
          'Color','magenta', 'LineWidth', 0.5);
     text(t(length(t)), y(length(y),2),...
     {sprintf('t=%d s',round(t(length(t)))), "\downarrow"},'Color','red','FontSize',9 ...
     ,'VerticalAlignment','bottom')
end
legend({['b=' num2str(b(b_val))]});
end
end