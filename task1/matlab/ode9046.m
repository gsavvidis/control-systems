%% ode function based on sytem equations
function [dy] = ode9046(y,b)
     global c;
     global d;
     global k;
     global M;
     dy=zeros(2,1);
     dy(1)=y(2);
     if (y(2)>=b)
               F_b1=(y(2)-b-c)^2+d;
     else
               F_b1=-(y(2)-b+c)^2-d;
     end
     dy(2)=(-F_b1-k*y(1))/M;

     if isnan(dy)
          return;
     end
end